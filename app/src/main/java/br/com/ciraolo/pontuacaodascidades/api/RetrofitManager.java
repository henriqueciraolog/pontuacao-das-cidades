package br.com.ciraolo.pontuacaodascidades.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by henriqueciraolo on 20/05/17.
 */

public class RetrofitManager {
    private static Services service;

    public static Services getRetrofit() {
        if (service == null) {
            service = new Retrofit.Builder()
                    .baseUrl("http://wsteste.devedp.com.br/Master/CidadeServico.svc/rest/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(Services.class);
        }
        return service;
    }
}
