package br.com.ciraolo.pontuacaodascidades.adapter;

//================================================================================
// IMPORTS
//================================================================================

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.ciraolo.pontuacaodascidades.R;
import br.com.ciraolo.pontuacaodascidades.model.Cidade;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CidadeRecyclerViewAdapter extends RecyclerView.Adapter<CidadeRecyclerViewAdapter.ViewHolder> {

    private List<Cidade> mDataset;
    private final OnItemClickListerner mListerner;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        //PROPERTIES
        @BindView(R.id.viewCidade)
        View viewCidade;
        @BindView(R.id.txvCidade)
        TextView txvCidade;
        @BindView(R.id.txvEstado)
        TextView txvEstado;

        ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    public CidadeRecyclerViewAdapter(List<Cidade> myDataset, OnItemClickListerner listerner) {
        mDataset = myDataset;
        mListerner = listerner;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_view_cidade, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Cidade cidade = mDataset.get(position);

        holder.viewCidade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListerner != null) mListerner.onItemClick(mDataset.get(position));
            }
        });

        holder.txvCidade.setText(cidade.getNome());
        holder.txvEstado.setText(cidade.getEstado());


    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void updateList(List<Cidade> newlist) {
        mDataset = new ArrayList<>();
        mDataset.addAll(newlist);
        notifyDataSetChanged();
    }

    public interface OnItemClickListerner {
        void onItemClick(Cidade cidade);
    }
}



