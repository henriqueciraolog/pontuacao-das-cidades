package br.com.ciraolo.pontuacaodascidades.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by henriqueciraolo on 20/05/17.
 */

public class Cidade implements Serializable {
    @SerializedName("Nome")
    private String nome;
    @SerializedName("Estado")
    private String estado;

    public String getEstado() {
        return estado;
    }

    public String getNome() {
        return nome;
    }
}
