package br.com.ciraolo.pontuacaodascidades;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PontuacaoActivity extends AppCompatActivity {

    @BindView(R.id.txvPontuacao)
    TextView txvPontuacao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pontuacao);
        ButterKnife.bind(this);

        getSupportActionBar().setTitle("Pontuação");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        if (intent == null) finish();

        Bundle bundle = intent.getExtras();
        if (bundle == null) finish();

        String cidade = bundle.getString("cidade");
        Integer pontuacao = bundle.getInt("pontuacao");

        if (cidade == null || pontuacao == null) finish();

        txvPontuacao.setText(getString(R.string.pontuacao, cidade, pontuacao));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
