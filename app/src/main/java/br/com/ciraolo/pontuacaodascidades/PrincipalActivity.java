package br.com.ciraolo.pontuacaodascidades;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.com.ciraolo.pontuacaodascidades.api.RetrofitManager;
import br.com.ciraolo.pontuacaodascidades.model.Cidade;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PrincipalActivity extends AppCompatActivity {

    @BindView(R.id.edtCidade)
    EditText edtCidade;

    @BindView(R.id.edtEstado)
    EditText edtEstado;

    private ProgressDialog progressDialog;

    private String cidadeBusca;
    private String estadoBusca;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        getSupportActionBar().setTitle(R.string.title_busca);
    }

    @OnClick(R.id.btnBuscar)
    public void buscar() {
        if (progressDialog == null || !progressDialog.isShowing()) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(getString(R.string.buscando));
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.show();

            cidadeBusca = StringUtils.stripAccents(edtCidade.getText().toString().toUpperCase());
            estadoBusca = StringUtils.stripAccents(edtEstado.getText().toString().toUpperCase());

            new FiltraCidades().execute();
        }
    }

    class FiltraCidades extends AsyncTask<Void, Void, List<Cidade>> {

        @Override
        protected List<Cidade> doInBackground(Void... voids) {
            try {
                // Uma explicação: Optei por utilizar a chamada sincrona do Retrofit em uma
                // Asynctask para não gerar chamadas aninhadas, uma vez que o filtro das cidades
                // ocorre nessa AsyncTask.
                Call<List<Cidade>> call = RetrofitManager.getRetrofit().buscarTodasCidades();
                Response<List<Cidade>> response = call.execute();
                List<Cidade> cidades = response.body();

                List<Cidade> cidadesFiltrado = new ArrayList<>();

                for (Cidade cidade : cidades) {
                    String cidadeObj = StringUtils.stripAccents(cidade.getNome().toUpperCase());
                    String estadoObj = StringUtils.stripAccents(cidade.getEstado().toUpperCase());
                    if (cidadeObj.contains(cidadeBusca) && estadoObj.contains(estadoBusca)) {
                        cidadesFiltrado.add(cidade);
                    }
                }
                return cidadesFiltrado;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<Cidade> cidadesFiltrado) {
            progressDialog.dismiss();
            if (cidadesFiltrado != null) {
                if (cidadesFiltrado.size() != 0) {
                    Intent intent = new Intent(PrincipalActivity.this, ResultadoCidadesActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("list", cidadesFiltrado.toArray(new Cidade[cidadesFiltrado.size()]));
                    intent.putExtras(bundle);
                    startActivity(intent);
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(PrincipalActivity.this);
                    builder.setTitle(R.string.ops);
                    builder.setMessage(R.string.nada_encontrado_2);
                    builder.setPositiveButton(R.string.ok, null);
                    builder.create().show();
                }
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(PrincipalActivity.this);
                builder.setTitle(R.string.ops);
                builder.setMessage(R.string.problema_rede);
                builder.setPositiveButton(R.string.ok, null);
                builder.create().show();
            }
        }
    }
}
