package br.com.ciraolo.pontuacaodascidades.api;

import java.util.List;

import br.com.ciraolo.pontuacaodascidades.model.Cidade;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by henriqueciraolo on 22/01/17.
 */

public interface Services {
        @GET("BuscaTodasCidades")
        Call<List<Cidade>> buscarTodasCidades();

        @POST("buscaPontos")
        Call<Integer> buscaPontos(@Body Cidade cidade);
}
