package br.com.ciraolo.pontuacaodascidades;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.ciraolo.pontuacaodascidades.adapter.CidadeRecyclerViewAdapter;
import br.com.ciraolo.pontuacaodascidades.api.RetrofitManager;
import br.com.ciraolo.pontuacaodascidades.model.Cidade;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResultadoCidadesActivity extends AppCompatActivity implements CidadeRecyclerViewAdapter.OnItemClickListerner, Callback<Integer> {

    @BindView(R.id.rcvResultado)
    RecyclerView rcvResultado;

    List<Cidade> dataSet;
    private CidadeRecyclerViewAdapter rcvAdapter;

    private ProgressDialog progressDialog;

    private Cidade cidade;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        ButterKnife.bind(this);

        getSupportActionBar().setTitle(R.string.title_resultado);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setupRecycleView();
        getData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getData() {
        Intent intent = getIntent();
        if (intent == null) finish();

        Bundle bundle = intent.getExtras();
        if (bundle == null) finish();

        Cidade[] cidades = (Cidade[]) bundle.getSerializable("list");
        if (cidades == null) finish();

        dataSet = Arrays.asList(cidades);
        rcvAdapter.updateList(dataSet);
    }

    private void setupRecycleView() {
        dataSet = new ArrayList<>();
        rcvResultado.setHasFixedSize(true);

        LinearLayoutManager rcvLayoutManager = new LinearLayoutManager(this);
        rcvResultado.setLayoutManager(rcvLayoutManager);

        //ADAPTER
        rcvAdapter = new CidadeRecyclerViewAdapter(dataSet, this);
        rcvResultado.setAdapter(rcvAdapter);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, LinearLayout.VERTICAL);
        rcvResultado.addItemDecoration(dividerItemDecoration);
    }

    @Override
    public void onItemClick(Cidade cidade) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.carregando));
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();

        this.cidade = cidade;

        Call<Integer> call = RetrofitManager.getRetrofit().buscaPontos(cidade);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<Integer> call, Response<Integer> response) {
        progressDialog.dismiss();
        Intent intent = new Intent(ResultadoCidadesActivity.this, PontuacaoActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("cidade", cidade.getNome());
        bundle.putInt("pontuacao", response.body());
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void onFailure(Call<Integer> call, Throwable t) {
        progressDialog.dismiss();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.ops);
        builder.setMessage(R.string.problema_rede);
        builder.setPositiveButton(R.string.ok, null);
        builder.create().show();
    }
}
